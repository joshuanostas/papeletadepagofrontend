import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NEW_EMPLEADO_FIJO, NEW_EMPLEADO_COMISION, NEW_EMPLEADO_POR_HORA } from '../shared/urls';
import { EmpleadoFijo } from '../models/empleado';
import { EmpleadoHora } from '../models/empleado-hora';
import { EmpleadoComision } from '../models/empleado-comision';

@Injectable({
  providedIn: 'root'
})
export class CrearEmpleadoService {

  constructor(private http: HttpClient) { }

  crearEmpleadoFijo(empleadoFijo: EmpleadoFijo) {
    const httpHeaders = new HttpHeaders({'Content-Type' : 'application/json'});
    const options = {headers: httpHeaders};
    console.log(JSON.stringify(empleadoFijo));
    return this.http.post(NEW_EMPLEADO_FIJO, JSON.stringify(empleadoFijo), options);
  }

  crearEmpleadoHora(empleadoHora: EmpleadoHora) {
    const httpHeaders = new HttpHeaders({'Content-Type' : 'application/json'});
    const options = {headers: httpHeaders};
    console.log(JSON.stringify(empleadoHora));
    return this.http.post(NEW_EMPLEADO_POR_HORA, JSON.stringify(empleadoHora), options);
  }

  crearEmpleadoComision(empleadoHora: EmpleadoComision) {
    const httpHeaders = new HttpHeaders({'Content-Type' : 'application/json'});
    const options = {headers: httpHeaders};
    console.log(JSON.stringify(empleadoHora));
    return this.http.post(NEW_EMPLEADO_COMISION, JSON.stringify(empleadoHora), options);
  }
}
