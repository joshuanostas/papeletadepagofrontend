import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { GENERAR_PAPELETAS } from '../shared/urls';

@Injectable({
  providedIn: 'root'
})
export class GenerarPapeletasService {

  generarPapeletasURL = GENERAR_PAPELETAS;

  constructor(private http: HttpClient) { }

  generarPapeletas(fecha: string) {
    let val = new Date(fecha);
    val.setDate(val.getDate() + 1)
    const url = `${this.generarPapeletasURL}?fecha=${val.toDateString()}`;
    return this.http.get(url);
  }
}
