import { TestBed } from '@angular/core/testing';

import { GenerarPapeletasService } from './generar-papeletas.service';

describe('GenerarPapeletasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenerarPapeletasService = TestBed.get(GenerarPapeletasService);
    expect(service).toBeTruthy();
  });
});
