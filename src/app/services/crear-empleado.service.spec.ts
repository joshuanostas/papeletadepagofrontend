import { TestBed } from '@angular/core/testing';

import { CrearEmpleadoService } from './crear-empleado.service';

describe('CrearEmpleadoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrearEmpleadoService = TestBed.get(CrearEmpleadoService);
    expect(service).toBeTruthy();
  });
});
