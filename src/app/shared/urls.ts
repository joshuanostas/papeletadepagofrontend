//crear empleado
export const NEW_EMPLEADO_FIJO = 'http://localhost:3000/empleadoFijo/nuevo';
export const NEW_EMPLEADO_COMISION = 'http://localhost:3000/empleadoComision/nuevo';
export const NEW_EMPLEADO_POR_HORA = 'http://localhost:3000/empleadoHora/nuevo';
//generar papeletas
export const GENERAR_PAPELETAS = 'http://localhost:3000/generarPapeletaDePago';