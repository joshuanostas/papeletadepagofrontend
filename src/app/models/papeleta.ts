export class Papeleta {

    nombre: string;
    salario: number;
    fecha: string;

    constructor() {
        this.nombre = '';
        this.salario = 0;
        this.fecha = '';
     }
}