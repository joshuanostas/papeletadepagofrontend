export class EmpleadoFijo {
        public nombre: string;
        public ci: number;
        public salario: number;
        public horaLlegada: string;
        public horaSalida: string;
        public asistencia: any[];
        public mediosDeNotificacion: any[];
        public metodoDePago: string;
        public celular: number;
        public email: string;
        public sindicato: string;
}
