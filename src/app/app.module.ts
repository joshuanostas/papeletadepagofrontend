import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PaginaPrincipalComponent } from './components/pagina-principal/pagina-principal.component';
import { CrearEmpleadoComponent } from './components/crear-empleado/crear-empleado.component';
import { GenerarPapeletasFormComponent } from './components/generar-papeletas-form/generar-papeletas-form.component';
import { GenerarPapeletasViewComponent } from './components/generar-papeletas-view/generar-papeletas-view.component';
import { EmpleadoFijoFormComponent } from './empleado-fijo-form/empleado-fijo-form.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { EmpleadoHoraFormComponent } from './empleado-hora-form/empleado-hora-form.component';
import { EmpleadoComisionFormComponent } from './empleado-comision-form/empleado-comision-form.component';
@NgModule({
  declarations: [
    AppComponent,
    PaginaPrincipalComponent,
    CrearEmpleadoComponent,
    GenerarPapeletasFormComponent,
    GenerarPapeletasViewComponent,
    EmpleadoFijoFormComponent,
    EmpleadoHoraFormComponent,
    EmpleadoComisionFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
