import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpleadoHora } from '../models/empleado-hora';
import { CrearEmpleadoService } from '../services/crear-empleado.service';

@Component({
  selector: 'app-empleado-hora-form',
  templateUrl: './empleado-hora-form.component.html',
  styleUrls: ['./empleado-hora-form.component.css']
})
export class EmpleadoHoraFormComponent implements OnInit {

  formEmpleadoHora: FormGroup;
  mediosDeNotificacion = [];
  metodosDePago = ['Pago en Cheque', 'Pago en Deposito', 'Pago en Efectivo'];
  perteneceSindicato = ['Pertenece', 'No Pertenece'];
  dropdownList = [];
  dropdownSettings = {};
  constructor(private router: Router, private fb: FormBuilder, private crearEmpleadoService: CrearEmpleadoService) {
   }

  ngOnInit() {
    this.dropdownList = [
      { item_id: 1, item_text: 'Whatsapp' },
      { item_id: 2, item_text: 'Email' },
      { item_id: 3, item_text: 'SMS' },
      { item_id: 4, item_text: 'Facebook' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: false
    };
    this.crearFormEmpleadoHora();
  }

  onItemSelect(item: any) {
    this.mediosDeNotificacion.push(item.item_text);
  }

  onItemDeSelect(item: any) {
    this.mediosDeNotificacion = this.mediosDeNotificacion.filter(medioDeNotificacion => medioDeNotificacion !== item.item_text);
  }

  crearFormEmpleadoHora() {
    this.formEmpleadoHora = this.fb.group({
      nombre: ['', Validators.required],
      ci: [null, Validators.required],
      salario: [null, Validators.required],
      horaLlegada: ['', Validators.required],
      horaSalida: ['', Validators.required],
      asistencia: [new Date().toLocaleDateString()],
      mediosDeNotificacion: ['', Validators.required],
      metodoDePago: ['', Validators.required],
      celular: ['', Validators.required],
      email: ['', Validators.required],
      sindicato: ['', Validators.required]
    });
  }

  onSubmit(formEmpleadoHora) {
    const empleadoHora = this.crearEmpleadoHora(formEmpleadoHora);
    console.log(empleadoHora);
    this.crearEmpleadoService.crearEmpleadoHora(empleadoHora).subscribe(response => console.log(response));
    this.router.navigate(['/']);
  }

  modelarMediosDeNotificacion(mediosDeNotificacion) {
    const mediosDeNotificacionEmpleado = [];
    mediosDeNotificacion.forEach(medioDeNotificacion => {
      mediosDeNotificacionEmpleado.push(medioDeNotificacion.item_text);
    });
    return mediosDeNotificacionEmpleado;
  }

  transformarSalarioANumero(salario) {
    return Number(salario);
  }

   // tslint:disable-next-line:adjacent-overload-signatures
   crearEmpleadoHora(formEmpleadoHora: any) {
    const empleadoHora = new EmpleadoHora();
    empleadoHora.nombre = formEmpleadoHora.nombre;
    empleadoHora.ci = formEmpleadoHora.ci;
    empleadoHora.salario = this.transformarSalarioANumero(formEmpleadoHora.salario);
    empleadoHora.horaLlegada = formEmpleadoHora.horaLlegada;
    empleadoHora.horaSalida = formEmpleadoHora.horaSalida;
    empleadoHora.asistencia = formEmpleadoHora.asistencia;
    empleadoHora.mediosDeNotificacion = this.modelarMediosDeNotificacion(formEmpleadoHora.mediosDeNotificacion);
    empleadoHora.metodoDePago = formEmpleadoHora.metodoDePago;
    empleadoHora.celular = formEmpleadoHora.celular;
    empleadoHora.email = formEmpleadoHora.email;
    empleadoHora.sindicato = formEmpleadoHora.sindicato;
    return empleadoHora;
  }

}
