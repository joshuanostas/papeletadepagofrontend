import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoHoraFormComponent } from './empleado-hora-form.component';

describe('EmpleadoHoraFormComponent', () => {
  let component: EmpleadoHoraFormComponent;
  let fixture: ComponentFixture<EmpleadoHoraFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadoHoraFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoHoraFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
