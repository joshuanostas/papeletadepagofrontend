import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmpleadoFijo } from '../models/empleado';
import { CrearEmpleadoService } from '../services/crear-empleado.service';

@Component({
  selector: 'app-empleado-fijo-form',
  templateUrl: './empleado-fijo-form.component.html',
  styleUrls: ['./empleado-fijo-form.component.css']
})
export class EmpleadoFijoFormComponent implements OnInit {

  formEmpleadoFijo: FormGroup;
  mediosDeNotificacion = [];
  metodosDePago = ['Pago en Cheque', 'Pago en Deposito', 'Pago en Efectivo'];
  perteneceSindicato = ['Pertenece', 'No Pertenece'];
  dropdownList = [];
  dropdownSettings = {};
  constructor(private router: Router, private fb: FormBuilder, private crearEmpleadoService: CrearEmpleadoService) {
   }

  ngOnInit() {
    this.dropdownList = [
      { item_id: 1, item_text: 'Whatsapp' },
      { item_id: 2, item_text: 'Email' },
      { item_id: 3, item_text: 'SMS' },
      { item_id: 4, item_text: 'Facebook' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: false
    };
    this.crearFormEmpleadoFijo();
  }

  onItemSelect(item: any) {
    this.mediosDeNotificacion.push(item.item_text);
  }

  onItemDeSelect(item: any) {
    this.mediosDeNotificacion = this.mediosDeNotificacion.filter(medioDeNotificacion => medioDeNotificacion !== item.item_text);
  }

  crearFormEmpleadoFijo() {
    this.formEmpleadoFijo = this.fb.group({
      nombre: ['', Validators.required],
      ci: [null, Validators.required],
      salario: [null, Validators.required],
      horaLlegada: ['', Validators.required],
      horaSalida: ['', Validators.required],
      asistencia: [new Date().toLocaleDateString()],
      mediosDeNotificacion: ['', Validators.required],
      metodoDePago: ['', Validators.required],
      celular: ['', Validators.required],
      email: ['', Validators.required],
      sindicato: ['', Validators.required]
    });
  }

  onSubmit(formEmpleadoFijo) {
    const empleadoFijo = this.crearEmpleadoFijo(formEmpleadoFijo);
    console.log(empleadoFijo);
    this.crearEmpleadoService.crearEmpleadoFijo(empleadoFijo).subscribe(response => console.log(response));
    this.router.navigate(['/']);
  }

  modelarMediosDeNotificacion(mediosDeNotificacion) {
    const mediosDeNotificacionEmpleado = [];
    mediosDeNotificacion.forEach(medioDeNotificacion => {
      mediosDeNotificacionEmpleado.push(medioDeNotificacion.item_text);
    });
    return mediosDeNotificacionEmpleado;
  }

  transformarSalarioANumero(salario) {
    return Number(salario);
  }

   // tslint:disable-next-line:adjacent-overload-signatures
   crearEmpleadoFijo(formEmpleadoFijo: any) {
    const empleadoFijo = new EmpleadoFijo();
    empleadoFijo.nombre = formEmpleadoFijo.nombre;
    empleadoFijo.ci = formEmpleadoFijo.ci;
    empleadoFijo.salario = this.transformarSalarioANumero(formEmpleadoFijo.salario);
    empleadoFijo.horaLlegada = formEmpleadoFijo.horaLlegada;
    empleadoFijo.horaSalida = formEmpleadoFijo.horaSalida;
    empleadoFijo.asistencia = formEmpleadoFijo.asistencia;
    empleadoFijo.mediosDeNotificacion = this.modelarMediosDeNotificacion(formEmpleadoFijo.mediosDeNotificacion);
    empleadoFijo.metodoDePago = formEmpleadoFijo.metodoDePago;
    empleadoFijo.celular = formEmpleadoFijo.celular;
    empleadoFijo.email = formEmpleadoFijo.email;
    empleadoFijo.sindicato = formEmpleadoFijo.sindicato;
    return empleadoFijo;
  }
}
