import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoFijoFormComponent } from './empleado-fijo-form.component';

describe('EmpleadoFijoFormComponent', () => {
  let component: EmpleadoFijoFormComponent;
  let fixture: ComponentFixture<EmpleadoFijoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadoFijoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoFijoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
