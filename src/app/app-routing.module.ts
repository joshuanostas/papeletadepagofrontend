import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaginaPrincipalComponent } from './components/pagina-principal/pagina-principal.component';
import { CrearEmpleadoComponent } from './components/crear-empleado/crear-empleado.component';
import { GenerarPapeletasFormComponent } from './components/generar-papeletas-form/generar-papeletas-form.component';
import { GenerarPapeletasViewComponent } from './components/generar-papeletas-view/generar-papeletas-view.component';
import { EmpleadoFijoFormComponent } from './empleado-fijo-form/empleado-fijo-form.component';
import { EmpleadoHoraFormComponent } from './empleado-hora-form/empleado-hora-form.component';
import { EmpleadoComisionFormComponent } from './empleado-comision-form/empleado-comision-form.component';

const routes: Routes = [
  {
    path: 'empleadoFijo',
    component: EmpleadoFijoFormComponent
  },
  {
    path: '',
    component: PaginaPrincipalComponent
  },
  {
    path: 'crearEmpleado',
    component: CrearEmpleadoComponent
  },
  {
    path: 'empleadoComision',
    component: EmpleadoComisionFormComponent
  },
  {
    path: 'empleadoHora',
    component: EmpleadoHoraFormComponent
  },
  {
    path: 'generarPapeletasForm',
    component: GenerarPapeletasFormComponent
  },
  {
    path: 'papeletas/:fecha',
    component: GenerarPapeletasViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
