import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-generar-papeletas-form',
  templateUrl: './generar-papeletas-form.component.html',
  styleUrls: ['./generar-papeletas-form.component.css']
})
export class GenerarPapeletasFormComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form) {
    this.router.navigate(['/papeletas/' + form.value.fecha]);
  }
}
