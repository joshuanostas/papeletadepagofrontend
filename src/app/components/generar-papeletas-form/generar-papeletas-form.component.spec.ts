import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarPapeletasFormComponent } from './generar-papeletas-form.component';

describe('GenerarPapeletasFormComponent', () => {
  let component: GenerarPapeletasFormComponent;
  let fixture: ComponentFixture<GenerarPapeletasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarPapeletasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarPapeletasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
