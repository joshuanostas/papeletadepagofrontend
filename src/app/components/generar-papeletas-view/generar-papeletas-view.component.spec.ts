import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarPapeletasViewComponent } from './generar-papeletas-view.component';

describe('GenerarPapeletasViewComponent', () => {
  let component: GenerarPapeletasViewComponent;
  let fixture: ComponentFixture<GenerarPapeletasViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarPapeletasViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarPapeletasViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
