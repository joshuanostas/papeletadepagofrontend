import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Papeleta } from '../../models/papeleta';
import { GenerarPapeletasService } from '../../services/generar-papeletas.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-generar-papeletas-view',
  templateUrl: './generar-papeletas-view.component.html',
  styleUrls: ['./generar-papeletas-view.component.css']
})
export class GenerarPapeletasViewComponent implements OnInit {

  papeletas = null;
  constructor(private route: ActivatedRoute, private generarPapeletaService: GenerarPapeletasService) { }

  ngOnInit() {
    this.generarPapeletaService.generarPapeletas(this.route.snapshot.params['fecha']).forEach(element => {
      this.papeletas = element;
    });

  }
}
