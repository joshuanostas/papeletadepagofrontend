import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoComisionFormComponent } from './empleado-comision-form.component';

describe('EmpleadoComisionFormComponent', () => {
  let component: EmpleadoComisionFormComponent;
  let fixture: ComponentFixture<EmpleadoComisionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadoComisionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoComisionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
