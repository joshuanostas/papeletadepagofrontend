import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrearEmpleadoService } from '../services/crear-empleado.service';
import { EmpleadoComision } from '../models/empleado-comision';

@Component({
  selector: 'app-empleado-comision-form',
  templateUrl: './empleado-comision-form.component.html',
  styleUrls: ['./empleado-comision-form.component.css']
})
export class EmpleadoComisionFormComponent implements OnInit {

  formEmpleadoComision: FormGroup;
  mediosDeNotificacion = [];
  metodosDePago = ['Pago en Cheque', 'Pago en Deposito', 'Pago en Efectivo'];
  perteneceSindicato = ['Pertenece', 'No Pertenece'];
  ventas = [{fecha: '06-06-13', monto: 0}];
  dropdownList = [];
  dropdownSettings = {};
  constructor(private router: Router, private fb: FormBuilder, private crearEmpleadoService: CrearEmpleadoService) {
   }

  ngOnInit() {
    this.dropdownList = [
      { item_id: 1, item_text: 'Whatsapp' },
      { item_id: 2, item_text: 'Email' },
      { item_id: 3, item_text: 'SMS' },
      { item_id: 4, item_text: 'Facebook' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: false
    };
    this.crearFormEmpleadoComision();
  }

  onItemSelect(item: any) {
    this.mediosDeNotificacion.push(item.item_text);
  }

  onItemDeSelect(item: any) {
    this.mediosDeNotificacion = this.mediosDeNotificacion.filter(medioDeNotificacion => medioDeNotificacion !== item.item_text);
  }

  crearFormEmpleadoComision() {
    this.formEmpleadoComision = this.fb.group({
      nombre: ['', Validators.required],
      ci: [null, Validators.required],
      salario: [null, Validators.required],
      horaLlegada: ['', Validators.required],
      horaSalida: ['', Validators.required],
      asistencia: [new Date().toLocaleDateString()],
      mediosDeNotificacion: ['', Validators.required],
      metodoDePago: ['', Validators.required],
      celular: ['', Validators.required],
      email: ['', Validators.required],
      sindicato: ['', Validators.required],
      comision: ['', Validators.required]
    });
  }

  onSubmit(formEmpleadoComision) {
    const empleadoComision = this.crearEmpleadoComision(formEmpleadoComision);
    console.log(empleadoComision);
    this.crearEmpleadoService.crearEmpleadoComision(empleadoComision).subscribe(response => console.log(response));
    this.router.navigate(['/']);
  }

  modelarMediosDeNotificacion(mediosDeNotificacion) {
    const mediosDeNotificacionEmpleado = [];
    mediosDeNotificacion.forEach(medioDeNotificacion => {
      mediosDeNotificacionEmpleado.push(medioDeNotificacion.item_text);
    });
    return mediosDeNotificacionEmpleado;
  }

  transorfmarANumero(datoNumerico) {
    return Number(datoNumerico);
  }

   // tslint:disable-next-line:adjacent-overload-signatures
   crearEmpleadoComision(formEmpleadoComision: any) {
    const empleadoComision = new EmpleadoComision();
    empleadoComision.nombre = formEmpleadoComision.nombre;
    empleadoComision.ci = formEmpleadoComision.ci;
    empleadoComision.salario = this.transorfmarANumero(formEmpleadoComision.salario);
    empleadoComision.horaLlegada = formEmpleadoComision.horaLlegada;
    empleadoComision.horaSalida = formEmpleadoComision.horaSalida;
    empleadoComision.asistencia = formEmpleadoComision.asistencia;
    empleadoComision.mediosDeNotificacion = this.modelarMediosDeNotificacion(formEmpleadoComision.mediosDeNotificacion);
    empleadoComision.metodoDePago = formEmpleadoComision.metodoDePago;
    empleadoComision.celular = formEmpleadoComision.celular;
    empleadoComision.email = formEmpleadoComision.email;
    empleadoComision.sindicato = formEmpleadoComision.sindicato;
    empleadoComision.comision = this.transorfmarANumero(formEmpleadoComision.comision);
    empleadoComision.ventas = this.ventas;
    return empleadoComision;
  }
}
